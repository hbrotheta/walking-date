package com.hbrotheta.walkingdate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class WalkingDateApplication {

    public static void main(String[] args) {
        SpringApplication.run(WalkingDateApplication.class, args);
    }
    //실행시 패스워드인코딩할 메서드를 만든다.
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}
