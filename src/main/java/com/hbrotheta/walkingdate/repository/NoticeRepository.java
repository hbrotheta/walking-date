package com.hbrotheta.walkingdate.repository;

import com.hbrotheta.walkingdate.entity.Notice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

public interface NoticeRepository extends JpaRepository<Notice, Long> {

    Page<Notice> findAllByNoticeIsEnableAndDatePostGreaterThanEqualAndDatePostLessThanEqualOrderByIdDesc(Boolean noticeIsEnable, LocalDate dateStart, LocalDate dateEnd, Pageable pageable);

    Page<Notice> findAllByTitleContainingIgnoreCaseAndDatePostGreaterThanEqualAndDatePostLessThanEqualOrderByNoticeIsEnableDescIdDesc(String searchTitle, LocalDate dateStart, LocalDate dateEnd, Pageable pageable);

    Page<Notice> findAllByTitleContainingIgnoreCaseAndNoticeIsEnableAndDatePostGreaterThanEqualAndDatePostLessThanEqualOrderByIdDesc(String searchTitle, Boolean noticeIsEnable, LocalDate dateStart, LocalDate dateEnd, Pageable pageable);

}
