package com.hbrotheta.walkingdate.repository;

import com.hbrotheta.walkingdate.entity.Member;
import com.hbrotheta.walkingdate.enums.member.Penalty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    // 동일 아이디 확인용
    Optional<Member> findByUsername(String username);
    // 동일 아이디 확인용
    long countByUsername(String username);
    // 동일 휴대폰 확인용
    long countByPhone(String phone);
    // 동일 닉네임 확인용
    long countByNickName(String nickName);
    // 동일 닉네임 정보 확인용
    Optional<Member> findAllByNickName(String nickName);
    // 전체 회원(탈퇴 제외) 정보 조회
    Page<Member> findAllByIsEnabledOrderByIdAsc(boolean IsEnabled, Pageable pageable);
    // 블랙회원 정보 조회
    Page<Member> findAllByPenaltyOrderByIdAsc(Penalty penalty, Pageable pageable);
    // 별점순 정보 조회
    Page<Member> findAllByOrderByAvgStarRatingDesc(Pageable pageable);

    List<Member> findAllByWalkingAddressId1EqualsOrWalkingAddressId2EqualsOrWalkingAddressId3OrderByDateJoinDesc(long walkingAddressId1, long walkingAddressId2, long walkingAddressId3);
}
