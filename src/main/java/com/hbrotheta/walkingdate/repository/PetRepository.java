package com.hbrotheta.walkingdate.repository;

import com.hbrotheta.walkingdate.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PetRepository extends JpaRepository<Pet, Long> {
    List<Pet> findByMember_Id(long memberId);

    Optional<Pet> findByMember_IdAndId(long memberId, long Id);
}
