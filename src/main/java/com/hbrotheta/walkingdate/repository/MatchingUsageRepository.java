package com.hbrotheta.walkingdate.repository;

import com.hbrotheta.walkingdate.entity.MatchingUsage;
import com.hbrotheta.walkingdate.enums.matchingusage.MatchingStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MatchingUsageRepository extends JpaRepository<MatchingUsage, Long> {
    // 내가 신청한 매칭내역 조회
    List<MatchingUsage> findAllByApplyMemberId_IdEqualsAndMatchingStatusOrderByDateUpdateDesc(long applyMemberId, MatchingStatus matchingStatus);
    // 내가 받은 매칭내역 조회
    List<MatchingUsage> findAllByReceiveMemberId_IdEqualsAndMatchingStatusOrderByDateUpdateDesc(long applyMemberId, MatchingStatus matchingStatus);
}
