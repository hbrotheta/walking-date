package com.hbrotheta.walkingdate.controller.common;

import com.hbrotheta.walkingdate.exception.CAccessDeniedException;
import com.hbrotheta.walkingdate.exception.CAuthenticationEntryPointException;
import com.hbrotheta.walkingdate.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 권한이 없는 사용자가 들어오면 예외처리를 한다
 */
@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException();
    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CAuthenticationEntryPointException();
    }
}
