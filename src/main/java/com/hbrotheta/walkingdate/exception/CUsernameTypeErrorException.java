package com.hbrotheta.walkingdate.exception;

public class CUsernameTypeErrorException extends RuntimeException {
    public CUsernameTypeErrorException(String msg, Throwable t) {
        super(msg, t);
    }

    public CUsernameTypeErrorException(String msg) {
        super(msg);
    }

    public CUsernameTypeErrorException() {
        super();
    }
}

