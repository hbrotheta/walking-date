package com.hbrotheta.walkingdate.exception;

public class CUsernameUsingException extends RuntimeException {
    public CUsernameUsingException(String msg, Throwable t) {
        super(msg, t);
    }

    public CUsernameUsingException(String msg) {
        super(msg);
    }

    public CUsernameUsingException() {
        super();
    }
}
