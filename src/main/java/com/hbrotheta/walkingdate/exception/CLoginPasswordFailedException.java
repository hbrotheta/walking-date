package com.hbrotheta.walkingdate.exception;

public class CLoginPasswordFailedException extends RuntimeException {
    public CLoginPasswordFailedException(String msg, Throwable t) {
        super(msg, t);
    }

    public CLoginPasswordFailedException(String msg) {
        super(msg);
    }

    public CLoginPasswordFailedException() {
        super();
    }
}
