package com.hbrotheta.walkingdate.model.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class NoticeSearchRequest {
    @ApiModelProperty(notes = "검색 타이틀")
    private String searchTitle;

    @ApiModelProperty(notes = "조회 년도")
    @NotNull
    private Integer dateYear;

    @ApiModelProperty(notes = "조회 월")
    @NotNull
    private Integer dateMonth;


}
