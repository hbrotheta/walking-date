package com.hbrotheta.walkingdate.model.common;

import lombok.Getter;
import lombok.Setter;

/**
 * 단수 아이템 용도
 * @param <T> 제네릭 형태
 */
@Getter
@Setter
public class SingleResult<T> extends CommonResult {
    private T data;
}
