package com.hbrotheta.walkingdate.model.common;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 리스트 수량 정보 용도
 * @param <T> 제네릭 형태
 */
@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private List<T> list;

    private Long totalItemCount;

    private Integer totalPage;

    private Integer currentPage;
}
