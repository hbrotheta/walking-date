package com.hbrotheta.walkingdate.model.walkingaddress;

import com.hbrotheta.walkingdate.entity.Member;
import com.hbrotheta.walkingdate.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WalkingAddressUserListResponse {
    @ApiModelProperty(value = "회원 시퀀스")
    private Long id;

    @ApiModelProperty(value = "회원 사진")
    private String memberProfileImage;

    @ApiModelProperty(value = "닉네임")
    private String nickName;

    @ApiModelProperty(value = "성별")
    private String  gender;

    @ApiModelProperty(value = "평점")
    private String avgStarRating;

    @ApiModelProperty(value = "패널티")
    private String penalty;


    public WalkingAddressUserListResponse(WalkingAddressUserListResponseBuilder builder) {
        this.id = builder.id;
        this.memberProfileImage = builder.memberProfileImage;
        this.nickName = builder.nickName;
        this.gender = builder.gender;
        this.avgStarRating = builder.avgStarRating;
        this.penalty = builder.penalty;
    }
    public static class WalkingAddressUserListResponseBuilder implements CommonModelBuilder<WalkingAddressUserListResponse> {
        private final Long id;
        private final String memberProfileImage;
        private final String nickName;
        private final String gender;
        private final String avgStarRating;
        private final String penalty;

        public WalkingAddressUserListResponseBuilder(Member member) {
            this.id = member.getId();
            this.memberProfileImage = member.getMemberProfileImage();
            this.nickName = member.getNickName();
            this.gender = member.getGender().getName();
            this.avgStarRating = String.format("%.1f", member.getAvgStarRating());
            this.penalty = member.getPenalty().getName();
        }

        @Override
        public WalkingAddressUserListResponse build() {
            return new WalkingAddressUserListResponse(this);
        }
    }
}

