package com.hbrotheta.walkingdate.model.matchingusage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MyMatchingAcceptRequest {
    @ApiModelProperty(value = "수락한 매칭내역 시퀀스")
    @NotNull
    private Long MatchingUsageAcceptId;

    @ApiModelProperty(value = "약속 시간")
    @NotNull
    private String datePromise;

    @ApiModelProperty(value = "남길 메세지")
    @Length(max = 50)
    private String acceptMessage;
}
