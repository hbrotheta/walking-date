package com.hbrotheta.walkingdate.entity;

import com.hbrotheta.walkingdate.enums.login.MemberGroup;
import com.hbrotheta.walkingdate.enums.member.Gender;
import com.hbrotheta.walkingdate.enums.member.Penalty;
import com.hbrotheta.walkingdate.interfaces.CommonModelBuilder;
import com.hbrotheta.walkingdate.model.member.MemberCreateRequest;
import com.hbrotheta.walkingdate.model.member.NickNameChangeRequest;
import com.hbrotheta.walkingdate.model.walkingaddress.WalkingAddressUserFavoritesRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

import static com.hbrotheta.walkingdate.lib.CommonDate.getNowTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member implements UserDetails {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "아이디")
    @Column(nullable = false, unique = true, length = 30)
    private String username;
    @ApiModelProperty(value = "비밀번호")
    @Column(nullable = false)
    private String password;
    @ApiModelProperty(value = "프로필 이미지")
    private String memberProfileImage;
    @ApiModelProperty(value = "닉네임")
    @Column(length = 20)
    private String nickName;
    @ApiModelProperty(value = "성별")
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @ApiModelProperty(value = "생년월일")
    @Column(nullable = false)
    private LocalDate birthDay;
    @ApiModelProperty(value = "연락처")
    @Column(nullable = false, unique = true, length = 20)
    private String phone;
    @ApiModelProperty(value = "권한 설정")
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;
    @ApiModelProperty(value = "회원 유/무")
    @Column(nullable = false)
    private Boolean isEnabled;
    @ApiModelProperty(value = "펫 유/무")
    @Column(nullable = false)
    private Boolean isPet;
    @ApiModelProperty(value = "가입시간")
    @Column(nullable = false)
    private LocalDateTime dateJoin;
    @ApiModelProperty(value = "수정시간")
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    @ApiModelProperty(value = "탈퇴시간")
    private LocalDateTime dateWithdraw;

    @ApiModelProperty(value = "평점")
    private Float avgStarRating;
    @ApiModelProperty(value = "즐겨찾는 산책 장소1")
    private Long walkingAddressId1;
    @ApiModelProperty(value = "즐겨찾는 산책 장소2")
    private Long walkingAddressId2;
    @ApiModelProperty(value = "즐겨찾는 산책 장소3")
    private Long walkingAddressId3;
    @ApiModelProperty(value = "패널티", notes = "(주의/경고/블랙리스트)")
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private Penalty penalty;

    // 회원가입 후 내가 즐겨찾는 장소 3곳 등록
    public void putMyWalkingAddressFavorites(WalkingAddressUserFavoritesRequest request) {
        this.walkingAddressId1 = request.getWalkingAddressId1();
        this.walkingAddressId2 = request.getWalkingAddressId2();
        this.walkingAddressId3 = request.getWalkingAddressId3();
    }
    // 닉네임 수정
    public void putNickName(NickNameChangeRequest request) {
        this.nickName = request.getNickName();
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }

    private Member(MemberBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.memberProfileImage = builder.memberProfileImage;
        this.nickName = builder.nickName;
        this.gender = builder.gender;
        this.birthDay = builder.birthDay;
        this.phone = builder.phone;
        this.memberGroup = builder.memberGroup;
        this.isEnabled = builder.isEnabled;
        this.isPet = builder.isPet;
        this.dateJoin = builder.dateJoin;
        this.dateUpdate = builder.dateUpdate;
        this.penalty = builder.penalty;
        this.avgStarRating = 0f;
    }
    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String username;
        private final String password;
        private final String memberProfileImage;
        private final String nickName;
        private final Gender gender;
        private final LocalDate birthDay;
        private final String phone;
        private final MemberGroup memberGroup;
        private final Boolean isEnabled;
        private final Boolean isPet;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateUpdate;
        private final Penalty penalty;
        // 빌더에서 회원그룹 따로 받는 이유 : 일반유저가 회원가입시 내가 일반유저다 라고 선택하지 않음.
        // 회원등록은 관리자페이지에서 관리자가 하거나 일반유저가 회원가입하거나.. N개의 경우의 수가 존재함.
        public MemberBuilder(MemberGroup memberGroup, MemberCreateRequest createRequest) {
            this.memberGroup = memberGroup;
            this.username = createRequest.getUsername();
            this.password = createRequest.getPassword();
            this.memberProfileImage = createRequest.getMemberProfileImage();
            this.nickName = createRequest.getNickName();
            this.gender = createRequest.getGender();
            this.birthDay = createRequest.getBirthDay();
            this.phone = createRequest.getPhone();
            this.isEnabled = true;
            this.isPet = false;
            this.dateJoin = getNowTime();
            this.dateUpdate = getNowTime();
            this.penalty = Penalty.NONE;
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
