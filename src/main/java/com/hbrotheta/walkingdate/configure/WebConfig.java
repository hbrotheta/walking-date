package com.hbrotheta.walkingdate.configure;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 웹설정 CORS 설정, 이미지파일 설정
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("*")
                .maxAge(3000);
    }

    /*
    로컬 서버 설정. 이 설정을 하지 않으면
    이미지가 업로드 되어도 서버를 다시 재시작해서
    업로드된 이미지를 서버시작과 동시에 알려주어야만
    이미지 접근이 됨.

    application.yml 파일에 추가된 설정되 같이 확인.
     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        String connectPath = "/static/**";
//        String resourcePath = "file:///C:/workspace/java/walking-date/src/main/resources/static/";
//        registry.addResourceHandler(connectPath)
//                .addResourceLocations(resourcePath);
//    }
}
