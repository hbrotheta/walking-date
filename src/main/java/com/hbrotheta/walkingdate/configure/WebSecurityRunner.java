package com.hbrotheta.walkingdate.configure;

import com.hbrotheta.walkingdate.service.member.MemberDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WebSecurityRunner implements ApplicationRunner { // 서버기동시 최초에 시작하는
    private final MemberDataService memberDataService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        memberDataService.setFirstMember();
    }
}
