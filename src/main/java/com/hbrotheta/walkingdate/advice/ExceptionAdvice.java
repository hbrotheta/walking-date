package com.hbrotheta.walkingdate.advice;

import com.hbrotheta.walkingdate.enums.common.ResultCode;
import com.hbrotheta.walkingdate.exception.*;
import com.hbrotheta.walkingdate.model.common.CommonResult;
import com.hbrotheta.walkingdate.service.common.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return  ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return  ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }
    @ExceptionHandler(CAuthenticationEntryPointException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPointException e) {
        return  ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT);
    }

    @ExceptionHandler(CUsernameSignInFailedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CUsernameSignInFailedException e) {
        return  ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED);
    }
    @ExceptionHandler(CUsernameTypeErrorException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CUsernameTypeErrorException e) {
        return  ResponseService.getFailResult(ResultCode.USERNAME_TYPE_ERROR);
    }
    @ExceptionHandler(CLoginPasswordFailedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CLoginPasswordFailedException e) {
        return  ResponseService.getFailResult(ResultCode.LOGIN_PASSWORD_FAILED);
    }
    @ExceptionHandler(CUsernameUsingException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CUsernameUsingException e) {
        return  ResponseService.getFailResult(ResultCode.USERNAME_USING);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }
    @ExceptionHandler(CPhoneNumberUsingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CPhoneNumberUsingException e) {
        return ResponseService.getFailResult(ResultCode.PHONE_NUMBER_USING);
    }
    @ExceptionHandler(CNickNameUsingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNickNameUsingException e) {
        return ResponseService.getFailResult(ResultCode.NICKNAME_USING);
    }
    @ExceptionHandler(CNickNameOverlapException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNickNameOverlapException e) {
        return ResponseService.getFailResult(ResultCode.NICKNAME_OVERLAP);
    }
    @ExceptionHandler(CApplyMemberOverlapException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CApplyMemberOverlapException e) {
        return ResponseService.getFailResult(ResultCode.APPLY_MEMBER_OVERLAP);
    }

}
