package com.hbrotheta.walkingdate.service.member;

import com.hbrotheta.walkingdate.entity.Member;
import com.hbrotheta.walkingdate.enums.login.MemberGroup;
import com.hbrotheta.walkingdate.enums.member.Gender;
import com.hbrotheta.walkingdate.enums.member.Penalty;
import com.hbrotheta.walkingdate.exception.*;
import com.hbrotheta.walkingdate.lib.CommonCheck;
import com.hbrotheta.walkingdate.model.common.ListResult;
import com.hbrotheta.walkingdate.model.member.MemberCreateRequest;
import com.hbrotheta.walkingdate.model.member.MemberInformationResponse;
import com.hbrotheta.walkingdate.model.member.NickNameChangeRequest;
import com.hbrotheta.walkingdate.model.member.NickNameResponse;
import com.hbrotheta.walkingdate.repository.MemberRepository;
import com.hbrotheta.walkingdate.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    /**
     * [관리자] 서버 기동시 슈퍼계정 자동생성
     */
    public void setFirstMember() {
        String username = "superadmin";
        String password = "qwert12345";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setNickName("최고관리자");
            createRequest.setGender(Gender.MAN);
            createRequest.setBirthDay(LocalDate.now());
            createRequest.setPhone("070-1234-1234");

            setMember(MemberGroup.ROLE_ADMIN, createRequest);
        }
    }

    /**
     * [일반유저/관리자] 회원가입 메서드
     * @param memberGroup 권한 설정
     * @param createRequest 회원가입 정보
     */
    public void setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CUsernameTypeErrorException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CLoginPasswordFailedException(); // 아이디 또는 비밀번호가 일치하지 않습니다.
        if (!isNewUsername(createRequest.getUsername())) throw new CUsernameUsingException(); // 중복된 아이디가 존재합니다 던지기
        if (!isNewPhone(createRequest.getPhone())) throw new CPhoneNumberUsingException(); // 이미 등록된 번호 입니다.
        if (!isNewNickName(createRequest.getNickName())) throw new CNickNameUsingException(); // 이미 등록된 닉네임 입니다.

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        Member member = new Member.MemberBuilder(memberGroup, createRequest).build();
        memberRepository.save(member);
    }

    /**
     * 회원가입시 동일 아이디 검증
     * @param username 아이디
     * @return ture/false
     */
    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }
    /**
     * 회원가입시 동일 휴대폰번호 검증
     * @param phone 휴대폰번호
     * @return ture/false
     */
    private boolean isNewPhone(String phone) {
        long dupCount = memberRepository.countByPhone(phone);
        return dupCount <= 0;
    }

    /**
     * 회원가입시 동일 닉네임 검증
     * @param nickName 닉네임
     * @return ture/false
     */
    private boolean isNewNickName(String nickName) {
        long dupCount = memberRepository.countByNickName(nickName);
        return dupCount <= 0;
    }

    /**
     * [일반유저] 신청받은 회원정보 가져오기
     * @param memberId 신청받은 회원 시퀀스
     * @return 신청받은 회원정보
     */
    public Member getMember(long memberId) {
        return memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
    }

    //todo 사진 이미지 get + put 배우면 작성하기

    /**
     * [일반유저] 마이페이지 닉네임 가져오는 메서드
     * @param member 회원 정보
     * @return 회원 닉네임
     */
    public NickNameResponse getMyNickName(Member member) {
        return new NickNameResponse.NickNameResponseBuilder(member).build();
    }

    /**
     * [일반유저] 변경할 닉네임을 검증한 후 저장 or 예외처리
     * @param member 회원 정보
     * @param request 변경할 닉네임
     */
    public void putMyNickName(Member member, NickNameChangeRequest request) {
        // 회원 테이블에서 요청한 닉네임을 찾고
        Optional<Member> nickNameVerify = memberRepository.findAllByNickName(request.getNickName());

        // 닉네임이 사용중이라면 예외처리 "닉네임이 사용중 입니다."
        if (nickNameVerify.isPresent()) throw new CNickNameUsingException();

        // 기존 닉네임과 동일한 닉네임 일 경우 예외처리 "동일한 닉네임으로는 변경이 불가합니다."
        if (member.getNickName().equals(request.getNickName())) throw new CNickNameOverlapException();

        // 아닐경우 닉네임을 수정 하고 저장
        member.putNickName(request);
        memberRepository.save(member);
    }

    /**
     * [관리자] 전체 회원정보 조회 (페이징처리)
     * @param pageNum 현재 페이지
     * @return 페이지정보 및 전체 회원정보
     */
    public ListResult<MemberInformationResponse> getMembers(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        Page<Member> members = memberRepository.findAllByIsEnabledOrderByIdAsc(true, pageRequest);

        List<MemberInformationResponse> result = new LinkedList<>();

        members.getContent().forEach(member -> {
            MemberInformationResponse addItem = new MemberInformationResponse.MemberInformationResponseBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result, members.getTotalElements(), members.getTotalPages(), members.getPageable().getPageNumber());
    }

    /**
     * [관리자] 블랙 회원정보 조회 (페이징처리)
     * @param pageNum 현재 페이지
     * @return 페이지정보 및 블랙 회원정보
     */
    public ListResult<MemberInformationResponse> getBlackMembers(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        Page<Member> members = memberRepository.findAllByPenaltyOrderByIdAsc(Penalty.BLACK_LIST, pageRequest);

        List<MemberInformationResponse> result = new LinkedList<>();

        members.getContent().forEach(member -> {
            MemberInformationResponse addItem = new MemberInformationResponse.MemberInformationResponseBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result, members.getTotalElements(), members.getTotalPages(), members.getPageable().getPageNumber());
    }

    /**
     * [관리자] 별점순 회원정보 조회 (페이징처리)
     * @param pageNum 현재 페이지
     * @return 페이지정보 및 별점순 회원정보
     */
    public ListResult<MemberInformationResponse> getStarPointMembers(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        Page<Member> members = memberRepository.findAllByOrderByAvgStarRatingDesc(pageRequest);

        List<MemberInformationResponse> result = new LinkedList<>();

        members.getContent().forEach(member -> {
            MemberInformationResponse addItem = new MemberInformationResponse.MemberInformationResponseBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result, members.getTotalElements(), members.getTotalPages(), members.getPageable().getPageNumber());
    }

    /**
     * 뷰 테스트용도
     * @return
     */
    public ListResult<MemberInformationResponse> getTest() {

        List<Member> members = memberRepository.findAll();

        List<MemberInformationResponse> result = new LinkedList<>();

        members.forEach(member -> {
            MemberInformationResponse addItem = new MemberInformationResponse.MemberInformationResponseBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

}
