package com.hbrotheta.walkingdate.service.matchingusage;

import com.hbrotheta.walkingdate.entity.MatchingUsage;
import com.hbrotheta.walkingdate.entity.Member;
import com.hbrotheta.walkingdate.entity.WalkingAddress;
import com.hbrotheta.walkingdate.exception.CApplyMemberOverlapException;
import com.hbrotheta.walkingdate.exception.CMissingDataException;
import com.hbrotheta.walkingdate.model.common.ListResult;
import com.hbrotheta.walkingdate.model.matchingusage.MyMatchingAcceptRequest;
import com.hbrotheta.walkingdate.model.member.MyMatchingListResponse;
import com.hbrotheta.walkingdate.repository.MatchingUsageRepository;
import com.hbrotheta.walkingdate.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

import static com.hbrotheta.walkingdate.enums.matchingusage.MatchingStatus.WAIT;

@Service
@RequiredArgsConstructor
public class MatchingUsageService {
    private final MatchingUsageRepository matchingUsageRepository;

    /**
     * [일반유저] 매칭 신청
     * @param applyMember 신청한 회원
     * @param receiveMember 신청받은 회원
     * @param walkingAddress 신청한 장소
     */
    public void setMatching(Member applyMember, Member receiveMember, WalkingAddress walkingAddress) {
        if (applyMember.getId().equals(receiveMember.getId())) throw new CApplyMemberOverlapException();
        MatchingUsage matchingUsage = new MatchingUsage.MatchingUsageCreateBuilder(applyMember, receiveMember, walkingAddress).build();
        matchingUsageRepository.save(matchingUsage);
    }

    /**
     * [일반유저] 내가 신청한 매칭내역
     * @param member 나의 정보
     * @return 내가 신청한 매칭내역
     */
    public ListResult<MyMatchingListResponse> getMyMatchingApplyList(Member member) {
        List<MatchingUsage> matchingUsages = matchingUsageRepository.findAllByApplyMemberId_IdEqualsAndMatchingStatusOrderByDateUpdateDesc(member.getId(), WAIT);

        List<MyMatchingListResponse> result = new LinkedList<>();

        matchingUsages.forEach(matchingUsage -> {
            MyMatchingListResponse addItem = new MyMatchingListResponse.MyMatchingApplyResponseBuilder(matchingUsage).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * [일반유저] 내가 받은 매칭내역
     * @param member 나의 정보
     * @return 내가 받은 매칭내역
     */
    public ListResult<MyMatchingListResponse> getMyMatchingReceiveList(Member member) {
        List<MatchingUsage> matchingUsages = matchingUsageRepository.findAllByReceiveMemberId_IdEqualsAndMatchingStatusOrderByDateUpdateDesc(member.getId(), WAIT);

        List<MyMatchingListResponse> result = new LinkedList<>();

        matchingUsages.forEach(matchingUsage -> {
            MyMatchingListResponse addItem = new MyMatchingListResponse.MyMatchingApplyResponseBuilder(matchingUsage).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * [일반유저] 나의 매칭 수락
     * @param request 매칭 수락시 필요 정보
     */
    public void putMyMatchingAccept(MyMatchingAcceptRequest request) {
        MatchingUsage matchingUsage = matchingUsageRepository.findById(request.getMatchingUsageAcceptId()).orElseThrow(CMissingDataException::new);
        matchingUsage.putAccept(request);
        matchingUsageRepository.save(matchingUsage);
    }


//    public void putMatching(Member member, long matchingUsageId, MatchingStatus matchingStatus, float starPoint) {
//        MatchingUsage matchingUsage = matchingUsageRepository.findById(matchingUsageId).orElseThrow(CMissingDataException::new);
//        matchingUsage.putMatchingStatus(matchingStatus, starPoint);
//        matchingUsageRepository.save(matchingUsage);
//
//        getAvgStarPoint(member);
//        Member.PutAvgStarPoint(float avgStarPoint)
//        memberRepository.save(member);
//    }
//
//    public float getAvgStarPoint(Member member) {
//        float avgStarPoint = matchingUsageRepository.findByid(member.getId());
//        평균 구하하고 리턴
//    }
}
