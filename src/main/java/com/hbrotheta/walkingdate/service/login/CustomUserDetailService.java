package com.hbrotheta.walkingdate.service.login;

import com.hbrotheta.walkingdate.entity.Member;
import com.hbrotheta.walkingdate.exception.CUsernameSignInFailedException;
import com.hbrotheta.walkingdate.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final MemberRepository memberRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member member = memberRepository.findByUsername(username).orElseThrow(CUsernameSignInFailedException::new);
        return member;
    }
}
