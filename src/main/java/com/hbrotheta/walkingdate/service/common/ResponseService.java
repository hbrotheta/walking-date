package com.hbrotheta.walkingdate.service.common;

import com.hbrotheta.walkingdate.enums.common.ResultCode;
import com.hbrotheta.walkingdate.model.common.CommonResult;
import com.hbrotheta.walkingdate.model.common.ListResult;
import com.hbrotheta.walkingdate.model.common.SingleResult;
import org.springframework.stereotype.Service;

@Service
public class ResponseService {
    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess) {
        if (isSuccess) setSuccessResult(result);
        else setFailResult(result);
        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) { // 무언가하나받을거야 리턴타입
        SingleResult<T> result = new SingleResult<>(); // SingleResult 무언가 타입을 result 에 초기화하고 빈공간을 생성
        result.setData(data); // data 값을 받고 data 에 세팅후 result 에 저장
        setSuccessResult(result); //setSuccessResult 메소드에 result 값 전달
        return result;
    }

    public static CommonResult getSuccessResult() {
        CommonResult result = new CommonResult();
        setSuccessResult(result);
        return result;
    }

    public static CommonResult getFailResult(ResultCode resultCode) {
        CommonResult result = new CommonResult();
        result.setIsSuccess(false);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMsg());
        return result;
    }

    private static void setSuccessResult(CommonResult result) {
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
    }

    private static void setFailResult(CommonResult result) {
        result.setIsSuccess(false);
        result.setCode(ResultCode.FAILED.getCode());
        result.setMsg(ResultCode.FAILED.getMsg());
    }
}
