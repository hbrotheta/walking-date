package com.hbrotheta.walkingdate.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
